package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 * 
 * @author Christian Charity
 *
 */
	public class Connector{
		
		/**
		 * sqlUrlDefault is the url to return to if the database does not exist.
		 */
	    private static String sqlUrl = "jdbc:mysql://localhost:3306/";
	    private static String sqlUrlDefault = "jdbc:mysql://localhost:3306/";
	    private final static String sqlUser = "root";
	    private final static String sqlPasswd = "";
	    private final static String sqlDriver = "com.mysql.jdbc.Driver";
	    private static Connection conn = null;
	    private static Statement stat = null;
	    private static String query = null;
	    
	    /**
	     * Constructor
	     * @param dbName
	     */
	    public Connector(String dbName){
	    		sqlUrl += dbName;
	    		try {
					Class.forName(sqlDriver);
					conn = DriverManager.getConnection(sqlUrl, sqlUser, sqlPasswd);
					if(conn.isValid(5000))
			    		System.out.println("Database is connected..");
					else
						System.out.println("Database NOT connected...");
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    }
	    
	    //////////////////////////////////////////////
	    //					Technical				//
	    //////////////////////////////////////////////  
	    
	    /**
	     * Sleep Connection
	     */
	    public void sleepConnection(){
	    	try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println("Cannot Sleep DB Connection");
			}
	    }
	    
	    /**
	     * Wake Connection
	     */
	    public void wakeConnection(){
			try {
				conn = DriverManager.getConnection(sqlUrl, sqlUser, sqlPasswd);
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println("Cannot Wake DB Connection");
			}

	    }
	    
	    //////////////////////////////////////////////
	    //					Users					//
	    ////////////////////////////////////////////// 
	    
	    /**
	     *  Does User Exist? In the future email or phone is better for checking credentials
	     * @param username
	     * @return
	     */
	    public String doesUserExist(String username, String email){
	    	String toReturn = "false";
			try {			
				ResultSet rs = conn.createStatement().executeQuery("SELECT personalID FROM " +
						"$AYF$.users WHERE username = '"+ username +"' OR email = '"+email+"' ;");
				if(rs.next()){
					toReturn = Integer.toString(rs.getInt(1));
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
	    return toReturn;
	    }
	    
	    /**
	     * Verify User
	     * this method uses both Email and Password. In the future also username
	     * @param email
	     * @param password
	     * @return
	     */
	    public String[] verifyUser(String emailOrUsername, String password){
	    	String[] toReturn = new String[3];
	    	toReturn[0] = "false";
			try {			
				ResultSet rs = conn.createStatement().executeQuery("SELECT personalID, username, email FROM " +
						"$AYF$.users WHERE (email =" +"'"+ emailOrUsername +"' OR username ='"+emailOrUsername+"') AND password LIKE BINARY '"+password+"';");
				if(rs.next()){
					toReturn[0] = Integer.toString(rs.getInt(1));
					toReturn[1] = rs.getString(2);
					toReturn[2] = rs.getString(3);
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
	    	return toReturn;
	    }
	    
	    /**
	     * Create User
	     * @param username
	     * @param phone
	     * @param password
	     * @param email
	     * @param pushToken
	     * @return
	     */
	    public boolean createUser(String username, String phone, String password, String email, String loggedIn, String pushToken){
	    	try{
	    		Statement stm = conn.createStatement();
	    		stm.execute("INSERT INTO $AYF$.users(username, phone, password, email, loggedIn,pushToken) " +
	    				"value('"+username+"', '"+phone+"', '"+password+"', '"+email+"', '"+loggedIn+"', '"+pushToken+"');");	
	    	}
	    	catch(SQLException e){
	    		e.printStackTrace();
	    		return false; // If something went wrong
	    	}
	    	return true; // If went well
	    }
	    
	    /**
	     * Get Push Token
	     * @param email
	     * @return
	     */
	    public String getPushToken(String email){
	    	String toReturn = "false";
			try {			
				ResultSet rs = conn.createStatement().executeQuery("SELECT pushToken FROM users WHERE email='"+email+"';");
				if(rs.next()){
					toReturn = rs.getString(1);
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
	    	return toReturn;
	    }
	  
	    //////////////////////////////////////////////
	    //				Questions					//
	    //////////////////////////////////////////////	    
	    
	    public String[] getArrayOfReceivers(String questionID){
	    	/** Data to return */
	    	String[] toReturn = null;
	    	String receivers = null;
	    	try{
	    		/** Get the current answers */
	    		ResultSet rs = conn.createStatement().executeQuery("SELECT receivers FROM $AYF$.postedQuestions WHERE questionID ="+questionID+";");
	    		if (rs.next()){
	    			receivers = rs.getString(1);
	    		}
	    		
	    		String[] people = receivers.split(":");
	    		toReturn = new String[people.length-1];
	    		for(int i=1; i<people.length;i++){
	    			toReturn[i-1] = people[i];
	    		}
	    		
	    	}catch(SQLException e){
		    	e.printStackTrace();
		    	toReturn = null;
	    	}
	    	return toReturn;
	    }
	    
	    /**
	     * Create Question
	     * closed is false and GateKeeper will receive the questionID.
	     * @param senderID
	     * @param questionText
	     * @param possibleAnswers
	     * @param receivers
	     */
	    public int createQuestion(String senderID, String username, String questionText, String possibleAnswers, String receivers, String actualAnswers, int closed, String dateAndTime){
	    	int questionIDToReturn = -1; // Just so its not null
	    	try{
	    		Statement stm = conn.createStatement();
	    		stm.execute("INSERT INTO $AYF$.postedQuestions(senderID, usernameOfSender, receivers, questionText, possibleAnswers, " +
	    					"actualAnswers, closed, dateAndTime) value("+senderID+",'"+username+"',  '"+receivers+"','"+questionText+"'," +
	    					" '"+possibleAnswers+"','"+actualAnswers+"', "+closed+", '"+dateAndTime+"' );");
	    		
	    		ResultSet rs = conn.createStatement().executeQuery("SELECT questionID FROM $AYF$.postedQuestions where senderID='"+senderID+"' order by questionID DESC");
	    		
	    		if(rs.next()){
	    			questionIDToReturn = (Integer)rs.getObject(1);
	    		}

	    	}
	    	catch(SQLException e){
	    		e.printStackTrace();
	    	}
	    	return questionIDToReturn;
	    }
	    


	    /**
	     * Get Questions that one user is a part of
	     * @param username
	     * @return
	     */
	    public String[][] getQuestions(String senderID, String emailAdress){
	    	/** Data to return */
	    	String[][] dataToReturn = null;
	    	try{
	    		/** Get the number of Questions */
		    	int numberOfQuestions = 0;
	    		ResultSet rs = conn.createStatement().executeQuery("SELECT COUNT(senderID) FROM $AYF$.postedQuestions" +
	    				" WHERE senderID="+senderID+" OR receivers LIKE '%:"+emailAdress+":%';");
	    		if(rs.next())
	    			numberOfQuestions = rs.getInt(1);
	    		
	    		/** Get the Questions */
	    		rs = null;
	    		rs = conn.createStatement().executeQuery("SELECT questionID, senderID, receivers, questionText, possibleAnswers, " +
	    				"actualAnswers, dateAndTime, usernameOfSender FROM $AYF$.postedQuestions WHERE senderID="+senderID+" OR receivers LIKE '%:"+emailAdress+":%';");
	    			    		
	    		dataToReturn = new String[numberOfQuestions][8];
	    		
	    		/** Get all the data into a 2 dimensional array */
	    		int count = 0;
	    		while(rs.next()){	    
	    			dataToReturn[count][0] = Integer.toString(rs.getInt(1));
	    			dataToReturn[count][1] = Integer.toString(rs.getInt(2));
	    			dataToReturn[count][2] = rs.getString(3);
	    			dataToReturn[count][3] = rs.getString(4);
	    			dataToReturn[count][4] = rs.getString(5);
	    			dataToReturn[count][5] = rs.getString(6);
	    			dataToReturn[count][6] = rs.getString(7);
	    			dataToReturn[count][7] = rs.getString(8);
	    			count++;
	    		}
	    		
	    		/** Tell 'GateKeeper' that table is empty */
	    		if(count==0){
	    			dataToReturn = null;
	    		}
	    	}
	    	catch(SQLException e){
	    		e.printStackTrace();
	    	}
	    	
	    	return dataToReturn;
	    }
	    
	    /**
	     * Check If User Already Answered The Question
	     * @param email
	     * @return
	     */
	    public boolean checkIfAlreadyAnswered(String questionID, String email){
	    	/** Data to return */
	    	boolean toReturn = false;
	    	try{
	    		/** Get the current answers */
	    		ResultSet rs = conn.createStatement().executeQuery("SELECT questionID FROM $AYF$.postedQuestions WHERE questionID ="+questionID+" "+
	    				" AND (actualAnswers LIKE '%,"+email+"%' OR actualAnswers LIKE '%:"+email+"%' OR actualAnswers LIKE '%"+email+":%');");
	    		if (rs.next()){
	    			toReturn = true;
	    		}
	    		
	    	}catch(SQLException e){
		    		e.printStackTrace();
			    	toReturn = false;
	    	}
	    	return toReturn;
	    }
	    
	    /**
	     * Answer Question
	     * @param username
	     * @param email
	     * @param questionID
	     * @param answer
	     * @return
	     */
	    public String answerQuestion(String email, String questionID, int answerIndex){
	    	/** Data to return */
	    	String toReturn = "false";
	    	String newAnswerString = ":0:0:";
	    		    	
	    	try{
	    		/** Get the current answers */
	    		ResultSet rs = conn.createStatement().executeQuery("SELECT actualAnswers FROM $AYF$.postedQuestions WHERE questionID ="+questionID+"");
	    		if (rs.next()){
	    			newAnswerString = rs.getString(1);
	    		}
	    		
	    		/** Editing Variables */
	  
	    		String[] actualAnswers = newAnswerString.split(":");
	    		String toEdit = actualAnswers[answerIndex];
	    		
	    		/** Append or overwrite? */
	    		if(toEdit.equals("0")){
	    			System.out.println("First time somebody answers this");
	    			toEdit = email;
	    		}
	    		else{
	    			toEdit = toEdit+","+email;	
	    		}
	    		actualAnswers[answerIndex] = toEdit;
	    			    		
	    		/** Assembling and saving actual answers */
	    		newAnswerString = ":"+actualAnswers[1]+":"+actualAnswers[2]+":";
	    		
	    		System.out.println("New answers= "+newAnswerString);
	    		
	    		/** Update in database */
	    		Statement stm = conn.createStatement();
	    		stm.execute("UPDATE $AYF$.postedQuestions SET actualAnswers='"+newAnswerString+"' WHERE questionID="+questionID+"");
	    		toReturn = "true";
	    	}
	    	catch(SQLException e){
	    		e.printStackTrace();
		    	toReturn = "false";
	    	}
	    	
	    	return toReturn;
	    	
	    }
	    
	    //////////////////////////////////////////////
	    //					Friends					//
	    //////////////////////////////////////////////
	    
	    /**
	     * Get All Friends
	     * returns a complete list of friends
	     * @param senderID
	     * @return
	     */
	    public String[][] getAllFriends(String senderID){
	    	
	    	/** String to return */
	    	String[][] dataToReturn = null;
	    	try{
	    		/** Get the number of Friends */
	    		int numberOfFriends = 0;
	    		ResultSet rs = conn.createStatement().executeQuery("SELECT COUNT(requesterEmail) FROM $AYF$.relationships " +
	    				"WHERE requesterEmail='"+senderID+"' OR receiverEmail LIKE '"+senderID+"';");
	    		
	    		/** Write number of friends */
	    		if(rs.next())
	    			numberOfFriends = rs.getInt(1);
    
	    		/** Get the Friends */
	    		rs = null;
	    		rs = conn.createStatement().executeQuery("SELECT requesterUsername, requesterEmail, receiverUsername, receiverEmail, status FROM " +
	    				"$AYF$.relationships WHERE (requesterEmail='"+senderID+"' OR receiverEmail LIKE '"+senderID+"');");
	    		/** Initialize variable to return */
	    		dataToReturn = new String[numberOfFriends][5];
    		
	    		/** Get all the data into a 2 dimensional array */
	    		int count = 0;
	    		while(rs.next()){	
	    			dataToReturn[count][0] = rs.getString(1);
	    			dataToReturn[count][1] = rs.getString(2);
	    			dataToReturn[count][2] = rs.getString(3);
	    			dataToReturn[count][3] = rs.getString(4);
	    			dataToReturn[count][4] = rs.getString(5);
	    			count++;
	    		}
    		
	    		/** Tell 'GateKeeper' that table is empty */
	    		if(count==0){
	    			dataToReturn = null;
    			}
    		}
	    	catch(SQLException e){
	    		e.printStackTrace();
	    	}
	    	return dataToReturn;
	    }
	    
	    /**
	     * Search For Friends
	     * this function returns a list with User data of the people
	     * best Matching the friendData description in either email or username
	     * @param friendData
	     * @return
	     */
	    public String[][] searchForFriends(String username, String email, String friendData){
	    	
	    	/** String to return */
	    	String[][] dataToReturn = null;
	    	try{
	    		/** Get the number of people */
	    		int numberMatchingPeople = 0;
	    		ResultSet rs = conn.createStatement().executeQuery("SELECT COUNT(email) FROM $AYF$.users " +
	    				"WHERE username LIKE '%"+friendData+"%'	OR email LIKE '%"+friendData+"%' OR phone LIKE '%"+friendData+"%' ;");
	    		
	    		/** Write number of people */
	    		if(rs.next())
	    			numberMatchingPeople = rs.getInt(1);
	    		
	    		/** Correcting for max number of people */
	    		if(numberMatchingPeople>5)
	    			numberMatchingPeople=5;
    
	    		/** Get the people */
	    		rs = null;
	    		rs = conn.createStatement().executeQuery("SELECT username, email FROM $AYF$.users WHERE username LIKE '%"+friendData+"%' " +
	    				"OR email LIKE '%"+friendData+"%' OR phone LIKE '%"+friendData+"%' ORDER BY username LIMIT 7;");
	    		
	    		/** Initialize variable to return */
	    		dataToReturn = new String[numberMatchingPeople][3];

	    		/** Get all the data into a 2 dimensional array */
	    		int count = 0;
	    		while(rs.next()){	
	    			dataToReturn[count][0] = rs.getString(1);
	    			dataToReturn[count][1] = rs.getString(2);
	    			dataToReturn[count][2] = "false";
	    			
	    			/** Check if status pending */
	    			ResultSet status = conn.createStatement().executeQuery("SELECT status FROM $AYF$.relationships WHERE (requesterEmail='"+email+"' AND receiverEmail='"+dataToReturn[count][1]+"')" +
		    			"OR (requesterEmail='"+dataToReturn[count][1]+"' AND receiverEmail='"+email+"');");
	    				    			
	    			if(status.next())
	    				dataToReturn[count][2] = status.getString(1);
	    			
	    			count++;
	    		}
    		
	    		/** Tell 'GateKeeper' that table is empty */
	    		if(count==0){
	    			dataToReturn = null;
    			}
	    	
    		}
	    	catch(SQLException e){
	    		e.printStackTrace();
	    	}
	    	return dataToReturn;
	    }
	    
	    /**
	     * Add Friend
	     * returns false if friend could n
	     * @param friendUsername
	     * @return
	     */
	    public String addFriend(String requesterUsername, String requesterEmail, String receiverUsername, String receiverEmail){
	    	/** String to return */
	    	String stringToReturn = "false";
	    	try{
	    		
	    		ResultSet rs = conn.createStatement().executeQuery("SELECT relationshipID FROM $AYF$.relationships WHERE (requesterEmail='"+requesterEmail+"' AND receiverEmail='"+receiverEmail+"')" +
	    				"OR (requesterEmail='"+receiverEmail+"' AND receiverEmail='"+requesterEmail+"');");
	    		
	    		if(rs.next()){
	    			System.out.println("Friendship already requested");
	    		}
	    		else{
	    		Statement stm = conn.createStatement();
	    		stm.execute("INSERT INTO $AYF$.relationships (requesterUsername, requesterEmail, receiverUsername, receiverEmail, status)" +
	    				"VALUE('"+requesterUsername+"', '"+requesterEmail+"', '"+receiverUsername+"', '"+receiverEmail+"', 'pending')");	
	    			stringToReturn = "true";
	   	    	}
	    		
    		}
	    	catch(SQLException e){
	    		e.printStackTrace();
	    	}
	    	return stringToReturn;
	    }
	    
	    /**
	     * Accept Friend Request
	     * @param email
	     * @param friendEmail
	     * @return
	     */
	    public String acceptFriendRequest(String email, String friendEmail){
	    	/** String to return */
	    	String stringToReturn = "true";
	    	try{
	    		
	    		/** Create Statement */
	    		Statement stm = conn.createStatement();
	    		stm.execute("UPDATE $AYF$.relationships SET status='accepted' WHERE requesterEmail='"+friendEmail+"' AND receiverEmail='"+email+"';");	
	    
    		}
	    	catch(SQLException e){
		    	stringToReturn = "false";
	    	}
	    	return stringToReturn;
	    }
	    
}
	   
	

	