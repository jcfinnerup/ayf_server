package logic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

/**
 * @author jcfinnerup
 */
public class JSONParser {
	
	// Variables
	private JSONArray jsonArrayAnswers;
	private JSONArray jsonArrayReceivers;
	private JSONObject jsonObject;
	private String senderID;
	private String questionText;
	private String command;
	private String[] possibleAnswers;
	private String[] receivers;
	private String receiversAsConnectedString;
	private String answersAsConnectedString;
	private String username;
	private String phoneNumber;
	private String email;
	private String password;
	private String deviceToken;
	private String friendData;
	private String friendUsername;
	private String friendEmail;
	private String emailOrUsername;
	private String questionID;
	private String answerIndex;
	
	/**
	 * Constructor
	 * Defines command and jsonObject to further read for getters and setters
	 * @param jsonString
	 */
	public JSONParser(String jsonString){
		// Might have trouble if empty stuff is received
		try {
			
			// JSON Objects
			jsonObject = new JSONObject(jsonString);
		
			// Only Direct Read
			command = jsonObject.getString("command");
		
		} catch (JSONException e) {
			System.out.println("Could not retrieve command from JSON String");
			
		}
	}
	
	/**
	 * Get Sender ID
	 * @return
	 */
	public String getSenderID(){
		try {
			senderID = jsonObject.getString("senderID");
		} catch (JSONException e) {
			System.out.println("Error In senderID");
		}
		return senderID;
	}
	
	/**
	 * Get Question Text
	 * @return
	 */
	public String getQuestionText(){
		try {
			questionText = jsonObject.getString("questionText");
		} catch (JSONException e) {
			System.out.println("Error In questionText");
		}
		return questionText;
	}

	/**
	 * Get Friend Data
	 * to search for friends
	 * @return
	 */
	public String getFriendData(){
		try {
			friendData = jsonObject.getString("friendData");
		} catch (JSONException e) {
			System.out.println("Error In friendData");
		}
		return friendData; 
	}
	
	/**
	 * Get Friend Username
	 * @return
	 */
	public String getFriendUsername(){
		try {
			friendUsername = jsonObject.getString("friendUsername");
		} catch (JSONException e) {
			System.out.println("Error In friendUsername");
		}
		return friendUsername;
	}
	
	/**
	 * Get Friend Email
	 * @return
	 */
	public String getFriendEmail(){
		try {
			friendEmail = jsonObject.getString("friendEmail");
		} catch (JSONException e) {
			System.out.println("Error In friendEmail");
		}
		return friendEmail;
	}
	
	/**
	 * Get Email Or Username
	 * @return
	 */
	public String getEmailOrUsername(){
		try {
			emailOrUsername = jsonObject.getString("emailOrUsername");
		} catch (JSONException e) {
			System.out.println("Error In emailOrUsername");
		}
		return emailOrUsername;
	}
	
	/**
	 * Get Question ID
	 * for answering questions
	 * @return
	 */
	public String getQuestionID(){
		try {
			questionID = jsonObject.getString("questionID");
		} catch (JSONException e) {
			System.out.println("Error In questionID");
		}
		return questionID;
	}
	
	/**
	 * Get Answer Index
	 * @return
	 */
	public String getAnswerIndex(){
		try {
			answerIndex = jsonObject.getString("answerIndex");
		} catch (JSONException e) {
			System.out.println("Error In answerIndex");
		}
		return answerIndex;
	}
	
	/**
	 * Get Command
	 * @return
	 */
	public String getCommand(){
		return command;
	}

	/**
	 * Get Answers Array
	 */
	public String[] getPossibleAnswers(){
		try {
			jsonArrayAnswers = jsonObject.getJSONArray("possibleAnswers");
			possibleAnswers = new String[jsonArrayAnswers.length()];
			for(int i=0; i<jsonArrayAnswers.length();i++){
				possibleAnswers[i]  = jsonArrayAnswers.getString(i);
			}
		} catch (JSONException e) {
			System.out.println("Error In Answer Array");
		}
		
		return possibleAnswers;
	}
	
	/**
	 * Get Receivers Array
	 * @return
	 */
	public String[] getReceivers(){
		try {
			jsonArrayReceivers = jsonObject.getJSONArray("receivers");
			receivers = new String[jsonArrayReceivers.length()];
			for(int i=0; i<jsonArrayReceivers.length();i++){
				receivers[i]  = jsonArrayReceivers.getString(i);
			}
		} catch (JSONException e) {
			System.out.println("Error In Receiver Array");
		}
		return receivers;
	}
	
	/**
	 * Get Receivers As Connected String;
	 * Takes the Array items and connects them as a String
	 * separated by ":"
	 * @return
	 */
	public String getReceiversAsConnectedString(){
		receiversAsConnectedString = ":";
		try {
			jsonArrayReceivers = jsonObject.getJSONArray("receivers");
			for(int i=0; i<jsonArrayReceivers.length();i++){
				receiversAsConnectedString += jsonArrayReceivers.getString(i);
				receiversAsConnectedString += ":";
			}
		} catch (JSONException e) {
			System.out.println("Error In Receivers As Connected String");
		}
		return receiversAsConnectedString;
	}
	
	public String getPossibleAnswersAsConnectedString(){
		answersAsConnectedString = ":";
		try {
			jsonArrayAnswers = jsonObject.getJSONArray("possibleAnswers");
			for(int i=0; i<jsonArrayAnswers.length();i++){
				answersAsConnectedString += jsonArrayAnswers.getString(i);
				answersAsConnectedString += ":";
			}
		} catch (JSONException e) {
			System.out.println("Error In Possible Answers As Connected String");
		}
		return answersAsConnectedString;
	}
	
	/**
	 * Get Username
	 * @return 
	 */
	public String getUsername(){
		try {
			username = jsonObject.getString("username");
		} catch (JSONException e) {
			System.out.println("Error In username");
		}
		return username;
	}
	
	/**
	 * Get Phone Number
	 * @return
	 */
	public String getPhoneNumber(){
		try {
			phoneNumber = jsonObject.getString("phoneNumber");
		} catch (JSONException e) {
			System.out.println("Error In phoneNumber");
		}
		return phoneNumber;
	}
	
	/**
	 * get Email
	 * @return
	 */
	public String getEmailAddress(){
		try {
			email = jsonObject.getString("email");
		} catch (JSONException e) {
			System.out.println("Error In email");
		}
		return email;
	}
	
	/**
	 * Get Password
	 * @return
	 */
	public String getPassword(){
		try {
			password = jsonObject.getString("password");
		} catch (JSONException e) {
			System.out.println("Error In password");
		}
		return password;
	}
	
	/**
	 * Get Device token
	 * @return
	 */
	public String getDeviceToken(){
		try {
			deviceToken = jsonObject.getString("deviceToken");
		} catch (JSONException e) {
			System.out.println("Error In deviceToken");
		}
		return deviceToken;
	}
	
	//////////////////////////////////////////////////
	//				REVERSE PARSE					//
	//////////////////////////////////////////////////
	
	public String conevertUserDataToJsonString(String[] loginArray) throws JSONException{
		
		/** Creating Master JSON object */ 
				
			/** Creating Content Object */
			JSONObject contObj = new JSONObject();
			contObj.put("userID", loginArray[0]);		// 1 - UserID STRING
			contObj.put("username", loginArray[1]);		// 2 - Username STRING
			contObj.put("email", loginArray[2]);		// 3 - Email STRING
			System.out.println(contObj.toString());
			
		return contObj.toString();
	}
	
	/**
	 * Convert Question Array to JSON String
	 * there are SEVEN(7) variables to be sent back to the client.
	 * this method THROWS because of 'GateKeeper' being aware of the error 
	 * is important so it can return proper value.
	 * @param toConvert
	 * @return
	 * @throws JSONException 
	 */
	public String convertQuestionArrayToJsonString(String[][] questionArray) throws JSONException{
		
		/** Creating Master JSON object */ 
		JSONArray masterObj = new JSONArray();
		
		/** Heavy loop to connect all questions in one string */
		for(int i=questionArray.length-1; i>=0; i--){
		
			/** Splitting Strings from SQL into an Array */
			String[] receiverData = questionArray[i][2].split(":");
			String[] possibleAnswersData = questionArray[i][4].split(":");
			String[] actualAnswerData = questionArray[i][5].split(":");
			
			/** Further Split Answered */
			String[] firstAnswers = actualAnswerData[1].split(",");
			String[] secondAnswers = actualAnswerData[2].split(",");
			
			
			/** Receiver Array */
			JSONArray receivers = new JSONArray();
			for(int n=1; n<receiverData.length ;n++)
				receivers.put(receiverData[n]);
		
			/** Possible Answers Array */
			JSONArray possibleAnswers = new JSONArray();
			possibleAnswers.put(possibleAnswersData[1]); // Because only 2 answer possibilities
			possibleAnswers.put(possibleAnswersData[2]); // Because only 2 answer possibilities
			
			/** Actual Answers Array */
			JSONArray actualAnswers = new JSONArray();
			actualAnswers.put(firstAnswers);	// Because only 2 answer possibilities
			actualAnswers.put(secondAnswers);	// Because only 2 answer possibilities
					
			/** Creating Content Object */
			JSONObject contObj = new JSONObject();
			contObj.put("questionID", questionArray[i][0]);			// 1 - Question ID 		INT
			contObj.put("senderID", questionArray[i][1]);			// 2 - Sender ID 		INT
			contObj.put("receivers", receivers);					// 3 - Receivers 		STRING[]
			contObj.put("questionText", questionArray[i][3]);		// 4 - Question Text 	STRING
			contObj.put("possibleAnswers", possibleAnswers);		// 5 - Possible Answers STRING[]
			contObj.put("actualAnswers", actualAnswers);			// 6 - Actual Answers 	STRING[]
			contObj.put("dateAndTime", questionArray[i][6]);		// 7 - Date And Time 	STRING
			contObj.put("usernameOfSender", questionArray[i][7]);	// 8 - Username Of Sender String

			masterObj.put(contObj);
			System.out.println(i+1+": "+contObj); 
		}
		return masterObj.toString();
	}
	
	/**
	 * Convert Friend List To JSON String
	 * @return
	 * @throws JSONException 
	 */
	public String convertFriendListToJsonString(String[][] friendArray) throws JSONException{
		
		/** Creating Master JSON object */ 
		JSONArray masterObj = new JSONArray();
		
		/** Heavy Loop to connect all friend in one string */
		for(int i=friendArray.length-1; i>=0; i--){
		
			/** Creating Content Object */
			JSONObject contObj = new JSONObject();
			contObj.put("requesterUsername", friendArray[i][0]);	// 1 - Requester Username STRING
			contObj.put("requesterEmail", friendArray[i][1]);		// 2 - Requester Email 	STRING
			contObj.put("receiverUsername", friendArray[i][2]);		// 3 - Receiver Username  STRING
			contObj.put("receiverEmail", friendArray[i][3]);		// 4 - Receiver Email 	STRING
			contObj.put("status", friendArray[i][4]);				// 5 - Status 	 	STRING
			masterObj.put(contObj);
			System.out.println(i+1+": "+contObj); 
		}
		return masterObj.toString();
	}
	
	/**
	 * Convert Search For Friends Data To JSON String
	 * @return
	 * @throws JSONException 
	 */
	public String convertSearchForFriendsToJsonString(String[][] potentialFriendArray) throws JSONException{
		
		/** Creating Master JSON object */ 
		JSONArray masterObj = new JSONArray();
		
		/** Heavy Loop to connect all people in one string */
		for(int i=potentialFriendArray.length-1; i>=0; i--){
		
			/** Creating Content Object */
			JSONObject contObj = new JSONObject();
			contObj.put("username", potentialFriendArray[i][0]);	// 1 - Username STRING
			contObj.put("email", potentialFriendArray[i][1]);		// 2 - Email STRING
			contObj.put("status", potentialFriendArray[i][2]);		// 3 - Status
			masterObj.put(contObj);
			System.out.println(i+1+": "+contObj); 
		}
		return masterObj.toString();
	}
	
}
