package logic;

import java.util.ArrayList;

import org.json.JSONException;

import db.Connector;

/**
 * Gate Keeper Class
 * @author jcfinnerup
 *
 *This Class will decide what to return for specific requests
 *and handle the rules for the database entries and potential error notifications. 
 */
public class GateKeeper {
	
	
	
	// Variables
	private Connector database;

	/**
	 * Constructor which initializes the push and database connections
	 * @param database
	 * @param push
	 */
	public GateKeeper(Connector database){
		this.database = database;
	}
	
	//////////////////////////////////////////////
	//				DB Connection				//
	//////////////////////////////////////////////
	
	/**
	 * Wake Connection
	 */
	public void wakeConnection(){
		database.wakeConnection();
	}
	
	/**
	 * Sleep Connection
	 */
	public void sleepConnection(){
		database.sleepConnection();
	}

	//////////////////////////////////////////////
	//				Push Bridge					//
	//////////////////////////////////////////////
	
	
	/**
	 * This is Used by gatekeeper to only push one place
	 * @param email
	 * @param Message
	 */
	private void pushBridge(String[] emails, String message){
		
		/** Arraylist to hold tokens */
		ArrayList<String> tokens = new ArrayList<String>();

		/** Does emails have tokens? */
		for(int i=0; i<emails.length;i++){
			String token = database.getPushToken(emails[i]);
			if(!token.equals("false")){
				tokens.add(token);
			}else{
				System.out.println("No Token To Push");
			}
		}	
		
		/**Single Array To Hold Tokens  */
		String[] tokenArray = new String[tokens.size()];
		tokenArray = tokens.toArray(tokenArray);
		
		
		/** Performing Push */
		PushNotify thread = new PushNotify(message, tokenArray);
		thread.start();
		
	}
	
	
	//////////////////////////////////////////////
	//					Users					//
	//////////////////////////////////////////////
	
	/**
	 * Create New User - Checks for problems
	 * @param JSONParser
	 * @return
	 */
	public String createNewUser(JSONParser json){
		String toReturn = "false";
		String doesUserExist = database.doesUserExist(json.getUsername(), json.getEmailAddress());
		if(doesUserExist.equals("false")){
			boolean newUserCreated = 	database.createUser(json.getUsername(), json.getPhoneNumber(), 
										json.getPassword(), json.getEmailAddress(), "true",json.getDeviceToken());
			
			/** Has the new user been created? Get the userID to send back*/
			if(newUserCreated == true){
				toReturn = database.doesUserExist(json.getUsername(), json.getEmailAddress());
				System.out.println(json.getUsername()+" is new user");
			}
		}
		else{
			System.out.println("Username Already Exists");
		}
		return toReturn;	
	}
	
	/**
	 * Verify User
	 * @param json
	 * @return
	 */
	public String verifyUser(JSONParser json){
		String toReturn = "false";
		String[] doesUserExist = database.verifyUser(json.getEmailOrUsername(), json.getPassword());
		if(!doesUserExist[0].equals("false")){
			try {
				toReturn = json.conevertUserDataToJsonString(doesUserExist);
				System.out.println("User Verified");
			} catch (JSONException e) {
				toReturn = "false";
				System.out.println("Error: User could not be verified");
			}
		}
		else{
			System.out.println("User could not be verified");
			toReturn = "false";
		}
		
		return toReturn;
	}
	
	//////////////////////////////////////////////
	//				Questions					//
	//////////////////////////////////////////////
	
	/**
	 * Create Question
	 * this is followed by a chain of events:
	 * 1. submitter will receive a code "questionID"
	 * 2. receivers will be notified via push and uponUpdate see the new data.
	 * @param JSONParser
	 * @return
	 */
	public String createQuestion(JSONParser json){
		/** Variables for new questions in database */
		String answersAtStart 	= ":0:0:"; // Works with 2 answers
		String dateAndTime		= "Sometime";
		int closedStatus 		= 0;
		
		int questionID = 	database.createQuestion(json.getSenderID(), json.getUsername(), json.getQuestionText(), 
							json.getPossibleAnswersAsConnectedString(), json.getReceiversAsConnectedString(), 
							answersAtStart, closedStatus, dateAndTime);
		
		/** Must Return a String */
		String toReturn = Integer.toString(questionID);
		if(!toReturn.equals("-1")){
			System.out.println("New question: "+json.getQuestionText());
			
			String[] emailsToFindTokens = database.getArrayOfReceivers(toReturn);
			
			pushBridge(emailsToFindTokens, json.getUsername()+": "+json.getQuestionText());			
		}
		else{
			System.out.println("Error When Creating Question");
		}
		return toReturn;
	}
	
	/**
	 * This return 
	 * @param json
	 * @return
	 */
	public String getAllQuestions(JSONParser json){
		/** Starting with empty */
		String toReturn = "empty";
	
		/** Reading 2 dimensional array of questions */
		String[][] questions = database.getQuestions(json.getSenderID(), json.getEmailAddress());
		System.out.println(questions);
		
		/** Make questions readable */
		if(questions != null){
			try {
				String jsonString = json.convertQuestionArrayToJsonString(questions);
				toReturn = jsonString;
			} catch (JSONException e) {
				System.out.println("Error Parsing Questions");
			}
		}
		else{
			System.out.println("No Questions for this user");
		}
		return toReturn;
	}
	
	/**
	 * Submit Answer
	 * @param json
	 * @return
	 */
	public String submitAnswer(JSONParser json){
		/** Starting with empty */
		String toReturn = "false";
		
		boolean isAlready = database.checkIfAlreadyAnswered(json.getQuestionID(), json.getEmailAddress());
		if(isAlready == true){
			System.out.println("Question already answered by user");
			toReturn = "false";
		}
		else{
			/** Attemp to answer question in database */
			toReturn = database.answerQuestion(json.getEmailAddress(), json.getQuestionID(), Integer.parseInt(json.getAnswerIndex()));
		}
		return toReturn;
	}
	
	//////////////////////////////////////////////
    //					Friends					//
    //////////////////////////////////////////////
    
	/**
	 * Pull All Friends
	 * will return all friends in list
	 * @param json
	 * @return
	 */
	public String getAllFriends(JSONParser json){
		/** Starting with empty */
		String toReturn = "empty";
		
		/** Fetching from database */
		String[][] friendList = database.getAllFriends(json.getEmailAddress());
		if(friendList != null){
			try{
				toReturn = json.convertFriendListToJsonString(friendList);
			}
			catch(JSONException e){
				System.out.println("Error Parsing Friendlis");
			}
		}
		else{
			System.out.println("No Friends for this user");
		}
		return toReturn;
	}

	/**
	 * Search for friends
	 * this is used to search for friends
	 * @param json
	 * @return
	 */
	public String searchForFriends(JSONParser json){
		/** String to return */
		String toReturn = "empty";
		
		/** Fetching Potential Friends From Database */
		String[][] potentialFriends = database.searchForFriends(json.getUsername(), json.getEmailAddress(), json.getFriendData());
		if(potentialFriends != null){
			try{
				toReturn = json.convertSearchForFriendsToJsonString(potentialFriends);
			}
			catch(JSONException e){
				System.out.println("Error Parsing Potential Friends");
			}
		}
		else{
			toReturn = "empty";
			System.out.println("No Users with that data");
		}
		return toReturn;
	}
	
	/**
	 * Add Friend
	 * takes username of friend
	 * @param json
	 * @return
	 */
	public String addFriend(JSONParser json){
		/** String to return */
		String toReturn = "false";
		
		/** Has friend been created */
		String hasFriendBeenCreated = database.addFriend(json.getUsername(), json.getEmailAddress(), 
				json.getFriendUsername(), json.getFriendEmail());
		toReturn = hasFriendBeenCreated;
		
		/** Should Push? */
		if(hasFriendBeenCreated.equals("true")){
			String token = database.getPushToken(json.getFriendEmail());
			/** Don't push if no token */
			
			if(!token.equals("false")){
				String[] tokens = new String[1];
				tokens[0] = token;
				PushNotify thread = new PushNotify(json.getUsername()+" has sent you a friend request", tokens);
				thread.start();
			}else{
				System.out.println("No Token To Push");
			}
			
		}
		
		return toReturn;
	}
	
	/**
	 * Accept Friend Request
	 * @param json
	 * @return
	 */
	public String acceptFriendRequest(JSONParser json){
		/** String to return */
		String toReturn = "false";
		
		/** Accept friend request */
		String hasBeenAccepted = database.acceptFriendRequest(json.getEmailAddress(), json.getFriendEmail());
		toReturn=hasBeenAccepted;
		
		/** Get Token If token */
		String token = database.getPushToken(json.getFriendEmail());
		
		/** Don't push if no token */
		if(!token.equals("false")){
			String[] tokens = new String[1];
			tokens[0] = token;
			PushNotify thread = new PushNotify("You're now friends with "+json.getUsername(), tokens);
			thread.start();
		}else{
			System.out.println("No Token To Push");
		}
		
		return toReturn;
	}
}

