package logic;

import org.apache.log4j.BasicConfigurator;
import org.json.JSONException;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;

public class PushNotify extends Thread{
	
	/** Variables */
	private String alert;
	private String[] tokens;
		
		
	public PushNotify(String alert, String[] tokens){
		System.out.println("Pushing..");
		/** Set alert & token */
		this.alert = alert;
		this.tokens = tokens;
	}
	
	
	// SEND SINGLE PUSH
	public void run(){
		try {		
			PushNotificationPayload payload = PushNotificationPayload.complex();
			payload.addAlert(alert);
			Push.payload(payload, "/home/flemming/Skrivebord/Certificates.p12", "AskYourFriends2014", true, tokens);
		} catch (CommunicationException e) {
			System.out.println("Error In Push Com");
		} catch (KeystoreException e) {
			System.out.println("Error In Push Key");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void combined(String message, String[] tokens){
		
	}

}
