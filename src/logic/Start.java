package logic;

import db.Connector;

public class Start {
	
	public static void main(String[] args){
		
		// Database Connector
		Connector database = new Connector("$AYF$");
		
		// Gate Keeper
		GateKeeper gateKeeper = new GateKeeper(database);
		
		// Server Init
		Server server = new Server(2013, gateKeeper);
		
		// Await Command
		while(true)
		server.awaitCommand();
		
	}
}
