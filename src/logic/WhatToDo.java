package logic;

/**
 * @author jcfinnerup
 */
public class WhatToDo {
	
	/**
	 * Variables & Objects
	 */
	@SuppressWarnings("unused")
	private JSONParser jsonParser;
	
	@SuppressWarnings("unused")
	private GateKeeper gateKeeper;
	
	/** The command number will be defined from the command received, or INVALID */
	private int commandNumber;
	
	/** Unified String to only return one thing */
	private String reply = null;
	
	/** Commands that can be understand */
	private String[] methods = 	{
									"INVALID_COMMAND", 			/** 0 */
									"PING", 					/** 1 */
									"CREATE_QUESTION", 			/** 2 */
									"CREATE_USER",				/** 3 */
									"PULL_QUESTIONS", 			/** 4 */
									"LOG_IN",					/** 5 */
									"PULL_FRIENDS",				/** 6 */
									"SEARCH_FRIENDS",			/** 7 */
									"ADD_FRIEND",				/** 8 */
									"ACCEPT_FRIEND_REQUEST",	/** 9 */
									"SUBMIT_ANSWER"				/** 10 */
								};
	
	/**
 * Constructor finding index of command from array methods
	 * @param jsonParser
	 * @param gateKeeper
	 */
	public WhatToDo(JSONParser jsonParser, GateKeeper gateKeeper){
		this.jsonParser = jsonParser;
		this.gateKeeper = gateKeeper;
		
		// Get the command index
		for(int i=0; i<methods.length;i++){
			if(jsonParser.getCommand().equals(methods[i])){
				commandNumber = i;
				break;
			}
		}
		System.out.println("Command: "+methods[commandNumber]);
	}
	
	/**
	 * Returns a reply to the server if action went well. 
	 * communicates with the database rule master GateKeeper. 
	 * This must start with verify user at some point. REMEMBER!
	 * @return
	 */
	public String doStuff(){
		gateKeeper.wakeConnection();
		switch(commandNumber){
			case 0: reply = "INVALID"; 									break;
			case 1: reply = "PING_BACK";								break;
			case 2: reply = gateKeeper.createQuestion(jsonParser);		break;
			case 3: reply = gateKeeper.createNewUser(jsonParser); 		break;
			case 4: reply = gateKeeper.getAllQuestions(jsonParser);	 	break;
			case 5: reply = gateKeeper.verifyUser(jsonParser);			break;
			case 6: reply = gateKeeper.getAllFriends(jsonParser);		break;
			case 7: reply = gateKeeper.searchForFriends(jsonParser);	break;
			case 8: reply = gateKeeper.addFriend(jsonParser);			break;
			case 9: reply = gateKeeper.acceptFriendRequest(jsonParser);	break;
			case 10: reply = gateKeeper.submitAnswer(jsonParser);		break;
		}
		gateKeeper.sleepConnection();
		return reply;
	}

}
