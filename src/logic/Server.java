package logic;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.ServerSocket;


public class Server {

	/**
	 *  Variables
	 */
	private Socket clientSocket;
	private ServerSocket serverSocket;
	private JSONParser jsonParser;
	private WhatToDo whatToDo;
	private GateKeeper gateKeeper;
	private BufferedReader in;
	private BufferedWriter out;
	
	/**
	 * Constructor
	 */
	public Server( int port, GateKeeper gateKeeper){
		this.gateKeeper = gateKeeper;
		try {
			this.serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Await Command 
	 */
	public void awaitCommand(){
		System.out.println("\nServer ready...");
		try{
			/** Prepare String To Be Returned*/
			String stringToReturn = null;
			
			/** Accepting Connection*/
			clientSocket = serverSocket.accept();	
			clientSocket.setSoTimeout(3000);
			
			/** Prepare Incoming & Outgoing Connections */
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
			
			/** Read Incoming String */
			String jsonString = in.readLine();
			jsonString = jsonString.replace("'","�");
			System.out.println(jsonString);

			/** Parse Incoming String */
			jsonParser = new JSONParser(jsonString);
			
			/** Check For Recognizable Command*/
			if(jsonParser.getCommand().equals("INVALID")){
				System.out.println("Invalid Data Received - Request Cancelled");
				stringToReturn = "INVALID";
			}
			else 
			{
				/** Decide Appropriate Action and Create Return Data*/
				whatToDo = new WhatToDo(jsonParser, gateKeeper);
				stringToReturn = whatToDo.doStuff();
			}
			
			/** Return String To Client */
			//System.out.println("Returning: "+stringToReturn);
			out.write(stringToReturn+"\n");
			out.flush();
			
			/** Closing Connections */
			clientSocket.close();
			
			
			/** Garbage Collect */
			// A Garbage Collect might solve transient problem

			// Catch Block
		} catch(IOException e){
			System.out.println("Error: IO. Potential timeout");
			e.printStackTrace();
		} catch(NullPointerException e){
			System.out.println("Error: Received NULL");
			e.printStackTrace();
		} catch(Exception e){
			System.out.println("Error: Undefined Exception");
			e.printStackTrace();
		}

	}
	
}
